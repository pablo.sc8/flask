#!/bin/bash

name=xtec-flask
group=xtec
location=westeurope

command -v az >/dev/null 2>&1 || {
    curl -L https://aka.ms/InstallAzureCli | bash
}

az account show >/dev/null 2>&1
if [ $? -ne 0 ]; then
    az login
fi

az group create --name $group --location $location
az appservice plan create --name xtec --resource-group $group --is-linux --sku F1
az webapp create --name $name --resource-group $group --plan xtec --runtime 'PYTHON|3.9'
az webapp config container set --docker-custom-image-name registry.gitlab.com/xtec/flask --name xtec-flask --resource-group $group

#`az webapp list --query '[].hostNames' --output tsv`